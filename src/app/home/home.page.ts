import { Component } from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {

    public _cards: any[];

    constructor() {
        this._cards = [
            {
                name: 'Roma',
                image: '',
                description: '',
                other: [{
                    name: 'Colosseo',
                    image: 'https://www.artribune.com/wp-content/uploads/2017/06/Il-Colosseo.jpg',
                    description: 'Il Colosseo, originariamente conosciuto come Anfiteatro Flavio, è il più grande anfiteatro del mondo',
                }, {
                    name: 'Piazza San Pietro',
                    image: 'https://serenamarletta.files.wordpress.com/2013/05/vaticano.jpg',
                    description: 'Posta a margine del centro storico di Roma la piazza fa parte della Città del Vaticano ed è delimitata dal confine con lo Stato italiano',
                }, {
                    name: 'Piazza Navona',
                    image: 'https://www.10cose.it/wp-content/uploads/2016/05/piazza-navona-roma.jpg',
                    description: 'Piazza Navona è una delle più celebri piazze monumentali di Roma, costruita in stile monumentale dalla famiglia Pamphili per volere di papa Innocenzo X ',
                }]
            },
            {
                name: 'Milano',
                image: '',
                description: '',
                other: [{
                    name: 'Duomo di Milano',
                    image: 'https://www.ilmilaneseimbruttito.com/wp-content/uploads/2018/05/shutterstock_700247896.jpg',
                    description: 'Il Duomo di Milano è la cattedrale dell\'arcidiocesi di Milano. Simbolo del capoluogo lombardo, e situato nell\'omonima piazza al centro della metropoli',
                }]
            },
            {
                name: 'Venezia',
                image: '',
                description: 'Venezia.... e venezia boh',
                other: [{
                    name: 'Piazza San Marco',
                    image: 'https://www.cronacadelveneto.com/wp-content/uploads/2018/03/piazza-san-marco-venezia-696x463.jpg',
                    description: 'Piazza San Marco, situata a Venezia, nominata in tutto il mondo per la sua bellezza e integrità architettonica è l\'unica piazza di Venezia',
                }]
            }
        ]
    }

}
