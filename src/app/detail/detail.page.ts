import { SharedService } from 'src/app/services/shared.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.page.html',
    styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

    public _detailData: any;

    constructor(
        private sharedService: SharedService
    ) { }

    ngOnInit() {
        this._detailData = this.sharedService.cardData;
    }

}
