import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { SharedService } from 'src/app/services/shared.service';

@Component({
    selector: 'custom-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {

    @Input() cardData: any;

    constructor(
        private router: Router,
        private sharedService: SharedService
    ) {
        console.log(this.cardData);
    }

    ngOnInit() { }


    goToDetail(data) {
        this.sharedService.cardData = data;
        console.log(this.sharedService.cardData);
        this.router.navigate(['/detail']);
    }

}
