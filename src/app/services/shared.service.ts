import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

    private _cardData: any;

    set cardData(_newCardData) {
        this._cardData = _newCardData;
    }

    get cardData() {
        return this._cardData;
    }

  constructor() { }
}
